#!/bin/sh
export LANG=en_US.UTF-8
MID=$(ps -p $PPID -o ppid=| sed s/[^0-9]*//g)

SSH_ID=$(tail /var/log/sshdusers.log | sed -ne "/sshd.$MID.:.*matching .SA key/{s/^.* //g;p;q}")
NAME_FILE=/home/pi/stratumkey/keys/names/$SSH_ID
if [ -f $NAME_FILE ]
then
  export NAME=$(cat $NAME_FILE)
else
  export NAME=$(who am i|awk '{ print $5}')
fi
