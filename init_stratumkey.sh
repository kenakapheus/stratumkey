# the following gpios are used:
# 7 (out): toggle to lock the door
# 11 (out): toggle to unlock the door
# 27 (in): reed contact for door open detection

echo 7 > /sys/class/gpio/export
echo 11 > /sys/class/gpio/export
echo 27 > /sys/class/gpio/export
echo "in" > /sys/class/gpio/gpio27/direction
echo "out" > /sys/class/gpio/gpio7/direction
echo "out" > /sys/class/gpio/gpio11/direction
chgrp stratumkey /sys/class/gpio/gpio7/value 
chgrp stratumkey /sys/class/gpio/gpio11/value 
chgrp stratumkey /sys/class/gpio/gpio27/value
chmod g+w /sys/class/gpio/gpio7/value
chmod g+w /sys/class/gpio/gpio11/value
touch /var/run/stratumkey
chgrp stratumkey /var/run/stratumkey
chmod g+w /var/run/stratumkey
