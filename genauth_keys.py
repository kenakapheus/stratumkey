#!/usr/bin/env python2

import glob
import os
import binascii
import sys

ansi_red = "\x1b[1;31m"
ansi_reset = "\x1b[0m"

for filename in glob.glob("/home/pi/stratumkey/keys/*.pub"):
	try:
		name = os.path.basename(filename)[:-4]
		data = file(filename).read()
		ssh_key_type, ssh_key, _ = data.split(" ",2)
		ssh_key.decode("base64")
		sys.stdout.write("%s %s %s\n" % (ssh_key_type, ssh_key, name))
		sys.stdout.flush()
	except BaseException:
		sys.stderr.write(ansi_red)
		sys.stderr.write("               _____ _    ___ _     _____ ____  \n")
		sys.stderr.write("              |  ___/ \  |_ _| |   | ____|  _ \ \n")
		sys.stderr.write("              | |_ / _ \  | || |   |  _| | | | |\n")
		sys.stderr.write("              |  _/ ___ \ | || |___| |___| |_| |\n")
		sys.stderr.write("              |_|/_/   \_\___|_____|_____|____/ \n\n")
		sys.stderr.write("==================== YOUR PUSH HAS FAILED! ====================\n\n")
		sys.stderr.write(ansi_reset)
		sys.stderr.write("Usually that happens because one of your keys has an invalid format.\n")
		sys.stderr.write("Make sure it conforms to the following format:\n\n")
		sys.stderr.write("    ssh-rsa AAAAB3NzaC1yc... user@host\n\n")
		sys.stderr.write("The following error happened in file %s:\n\n" % filename)
		sys.stderr.flush()
		raise
