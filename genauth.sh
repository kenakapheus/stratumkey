#!/bin/bash

set -e

tmpfile="$(mktemp /tmp/stratumkey-genauth.XXXX)"

if ! /home/pi/stratumkey/genauth_keys.py > $tmpfile; then
	exit 127
fi

cat -- "$tmpfile" > /home/stratumkey/.ssh/authorized_keys

wc -l /home/stratumkey/.ssh/authorized_keys
FILES=/home/pi/stratumkey/keys/*.pub
for file in $FILES
do
	sed 's/.pub//' <<< $(basename $file) > "/home/pi/stratumkey/keys/names/$(ssh-keygen -lf ${file} | awk '{ print $2 }')"
done

rm -- $tmpfile
